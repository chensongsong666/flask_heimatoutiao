#app/main.py
from flask_toutiao.app import create_app

app=create_app('dev')
@app.route('/')
def route_app():
    '''定义根路由：获取所有路由规则'''
    return {rule.endpoint:rule.rule for rule in app.url_map.iter_rules()}