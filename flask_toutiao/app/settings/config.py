#app/settings/config
#项目的基本配置文件
class DefaultConfig(object):
    '''默认配置'''
    #mysql配置,Flask
    SQLALCHEMY_DATABASE_URI='mysql://root:mysql@127.0.0.1:3306/hm_topnews'  #链接地址
    SQLALCHEMY_TRACK_MODIFICATIONS=False   #是否追踪数据变化
    SQLALCHEMY_ECHO=False   #是否打印底层执行的sql
    #redis配置
    REDIS_HOST='localhost'   #ip
    REDIS_PORT=6379   #端口
config_dict={
    'dev':DefaultConfig
}