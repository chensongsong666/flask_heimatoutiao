#app/__init__.py
#1定义工厂函数
#2添加common路径
from flask import Flask

from flask_toutiao.app.settings.config import config_dict
from flask_toutiao.common.utils.constans import EXTRA_ENV_CONFIG
from flask_sqlalchemy import SQLAlchemy
from redis import StrictRedis


def create_flask_app(config_type):
    '''创建flask应用'''
    #创建flask应用
    app=Flask(__name__)
    #根据类型加载配置子类
    config_class=config_dict[config_type]
    #先加载默认配置
    app.config.from_object(config_class)
    #再加载额外配置
    app.config.from_envvar(EXTRA_ENV_CONFIG,silent=True)
    #返回应用
    return app

def create_app(config_type):
    # 创建应用和组件初始化
    #创建flask应用
    app=create_flask_app(config_type)
    #组建初始化
    register_extensions(app)
    return app
#创建SQLAIchemy对象
db=SQLAlchemy()
#redis数据库操作对象
redis_client=None #type StrictRedis
#定义register_extensions函数，进行组件初始化
def register_extensions(app):
    '''组件初始化'''
    #SQLALchemy组件初始化
    db.init_app(app)
    #redis组件初始化
    global redis_client
    #设置decode_responses=True redis获取到数据会自动电泳decode解码数据
    redis_host=app.config['REDIS_HOST']
    redis_port=app.config['REDIS_PORT']
    redis_client=StrictRedis(host=redis_host,port=redis_port,decode_responses=True)
